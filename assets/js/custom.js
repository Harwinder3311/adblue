(function($) {
  'use strict';

  // Testimonials
  // var testimonials = $('#testimonials');
  // if (testimonials.length > 0) {
  //   testimonials.owlCarousel({
  //     loop: true,
  //     margin: 0,
  //     items: 1,
  //     nav: false,
  //     dots: true
  //   })
  // }


  //Avoid pinch zoom on iOS
  document.addEventListener('touchmove', function(event) {
    if (event.scale !== 1) {
      event.preventDefault();
    }
  }, false);
})(jQuery)


//owl 2 js
    var owl = jQuery('.testimonies_block .owl-carousel');
      owl.owlCarousel({
            center:true,
            margin: 20,
            loop: false,
            nav: false,
            dots:false,
          autoplay:true,
          rewind:true,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 2
          },
         
          1000: {
            items: 2.5
          },
             1500: {
            items: 3.5
          }
        }
      })


jQuery(window).on('scroll' , function(){

    if(jQuery(window).scrollTop() >= 3)
        {
            jQuery('header').addClass('fadeInDown sticky');
        }
    else{
         jQuery('header').removeClass('fadeInDown sticky');
         jQuery('header').removeClass('fadeInDown sticky');
    }
});

 new WOW().init();

  AOS.init();
